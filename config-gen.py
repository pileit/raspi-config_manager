#!/usr/bin/python3
import re, json

menuFnRe = r'^do_[a-z_]*_menu ?\(\) ?{$'
menuDefRe = r'^\s*[A-Z]*=\$\(whiptail --title "[\w \-\(\)]*".* --menu "[\w ]*"'
menuEntryRe = r'^\s*"[A-Z][0-9][\w /-]*"'
elseLineRe = r'^\s*else$'

menus = {}
'''
name(--menu) : {
    "function": fn name,
    "description": <--title>,
    "options": {
        "O1": "title"
    }
}
'''
fnList = []
'''
options[op]={
    get: 
    do:
    menu:
    code:
    description:
}'''
'''
nop_list=["calc_wt_size() {"]
miscOpts=["deb_ver () {"]

def addFn(ln, fn=None):
    if fn is None:
        fn="misc"
    elif fn in ["get", "do"]:
        pass
    else:
        raise ValueError("unexpected FN type %s" %fn)

    if ln.startswith(fn+"_"):   # combine get/do operations
        op=ln[ln.find("_")+1:ln.find("(")].strip()
    else:
        op=ln[:ln.find("(")].strip()
    
    if op not in options.keys():
        options[op] = {}
    
    print(line[:line.find("(")])
    options[op][fn] = line[:line.find("(")].strip()
    return
'''
inMenu = False
thisMenu = {}
bootMenu_skipToElse = False
#with open("/usr/bin/raspi-config", "r") as script
with open("raspi-config", "r") as script:
    for line in script:
        if bootMenu_skipToElse:
            if re.search(elseLineRe, line):
                bootMenu_skipToElse = False
        elif inMenu:
            menuDefHit = re.search(menuDefRe, line)
            if menuDefHit:    # Menu title
                thisMenu["Title"] = menuDefHit.group(0).split('"')[1]
                thisMenu["Name"] = menuDefHit.group(0).split('"')[3]
                print("Menu: " + thisMenu["Name"] + "\n > Title: " + thisMenu["Title"] + \
                    "\n > Function: " + thisMenu["Function"])
                if thisMenu["Name"] not in menus.keys():
                    menus[thisMenu["Name"]] = {}
                    menus[thisMenu["Name"]]["Options"] = {}                    
            elif not re.search(menuEntryRe, line):  # not Menu option
                #print(thisMenu)
                #print(menus[thisMenu["Name"]])
                menus[thisMenu["Name"]]["Title"] = thisMenu["Title"]
                menus[thisMenu["Name"]]["Function"] = thisMenu["Function"]
                inMenu = False
                thisMenu = {}
            else:   # Menu option by deduction
                menuOpt = re.search(menuEntryRe, line).group(0).split('"')[1]
                print(" > Option: " + menuOpt)
                menus[thisMenu["Name"]]["Options"][menuOpt.split()[0]] = ' '.join(menuOpt.split()[1:])
        elif "_menu()" in line:   # menu function
            inMenu = True
            thisMenu["Function"] = line[:-1]
            if thisMenu["Function"].startswith("do_boot_menu"):
                bootMenu_skipToElse = True
                #breakpoint()
            #print(line)
        elif line.endswith("() {"):   # function definitions
            fnName = line[:line.find("(")].strip()
            print("Function: " + fnName)
            fnList.append(fnName)

print(json.dumps(menus, indent=3))